#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

#define STATUS_DATA_FRAME_SIZE 20
#define ERROR_DATA_FRAME_SIZE 5
#define INTERVAL_ONE_BYTE_FRAME 1
#define INTERVAL_BETWEEN_FRAMES 1000

#define BAUDRATE_BOARD_SERIAL 38400 // For arduino programming
#define BAUDRATE_VIRTUAL_SERIAL 38400 // For generate data frames

long randNumber;
byte START_FRAME_BYTE = 0xF0;
byte END_FRAME_BYTE = 0xAA;
unsigned long interval_one_byte_frame = 0;
unsigned long interval_between_frames = 0;
bool isUserFrendlySerialPreviewVisable = false;
byte incomingByte;
const int rs = 2, en = 3, d4 = 4, d5 = 5, d6 = 6, d7 = 7;

byte data_frame_status[STATUS_DATA_FRAME_SIZE];
byte data_frame_error[ERROR_DATA_FRAME_SIZE];

int data_frame_status_counter = 0;
int data_frame_error_counter = 0;
bool isDataFrameOpen = false;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
SoftwareSerial softSerial_0(10, 11); // RX, TX -> Generate Error Data Frame
SoftwareSerial softSerial_1(8, 9); // RX, TX -> Generate Status Data Frame

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(BAUDRATE_BOARD_SERIAL);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("Init Soft Serial 0");
  // set the data rate for the SoftwareSerial port
  softSerial_0.begin(BAUDRATE_VIRTUAL_SERIAL);
  softSerial_0.println("Soft Seril 0 OK");

  Serial.println("Init Soft Serial 1");
  // set the data rate for the SoftwareSerial port
  softSerial_1.begin(BAUDRATE_VIRTUAL_SERIAL);
  softSerial_1.println("Soft Seril 1 OK");

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("VentilAid");

}

byte * createStatusDataFrame(byte start_byte, byte mes_no, byte hi_press_0, byte hi_pres_1, byte lo_press_0, byte lo_press_1, byte breaths, byte breath_pro, byte air_vol_0, byte air_vol_1,
                            byte f0, byte f1, byte f2, byte f3, byte f4, byte f5, byte f6, byte f7, byte f8, byte end_byte ){
  static byte frame[STATUS_DATA_FRAME_SIZE];

  frame[0] = start_byte;
  frame[1] = mes_no;
  frame[2] = hi_press_0;
  frame[3] = hi_pres_1;
  frame[4] = lo_press_0;
  frame[5] = lo_press_1;
  frame[6] = breaths;
  frame[7] = breath_pro;
  frame[8] = air_vol_0;
  frame[9] = air_vol_1;
  frame[10] = f0;
  frame[11] = f1;
  frame[12] = f2;
  frame[13] = f3;
  frame[14] = f4;
  frame[15] = f5;
  frame[15] = f6;
  frame[17] = f7;
  frame[18] = f8;
  frame[19] = end_byte;
  
  return frame;
}

byte * createErrorDataFrame(byte start_byte, byte mes_no, byte err_code, byte f0, byte end_byte ){
  static byte frame[STATUS_DATA_FRAME_SIZE];

  frame[0] = start_byte;
  frame[1] = mes_no;
  frame[2] = err_code;
  frame[3] = f0;
  frame[4] = end_byte;
  
  return frame;
}

void sendDataFrame(byte * inFrame, int frameSize){
  if(isUserFrendlySerialPreviewVisable){
    Serial.print("|");
  }
  
   for(int i=0; i<frameSize; i++){
         if(isUserFrendlySerialPreviewVisable){
          Serial.print(inFrame[i]);
          Serial.print("|");
         }
        if(frameSize == STATUS_DATA_FRAME_SIZE){
          softSerial_1.write(inFrame[i]);
        }
        if(frameSize == ERROR_DATA_FRAME_SIZE){
          softSerial_0.write(inFrame[i]);
        }
        delay(INTERVAL_ONE_BYTE_FRAME);
    
  }
}

void lcdDisplay(String paramName, String paramValue){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Param: " + paramName);
  lcd.setCursor(0,1);
  lcd.print("Value: " + paramValue);
  
}

void lcdDisplayDataFrameStatus(){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("BRE:    BRE_PRO:");
  lcd.setCursor(0,1);
  lcd.print((int)data_frame_status[6]);
  lcd.setCursor(8,1);
  lcd.print((int)data_frame_status[7]);
  
}

void parseDataFrame(byte inSerialData){
      if(inSerialData == START_FRAME_BYTE || isDataFrameOpen){
      isDataFrameOpen = true;
      data_frame_status[data_frame_status_counter] = inSerialData;
      lcdDisplayDataFrameStatus();
      data_frame_status_counter++;
    }
    if(inSerialData == END_FRAME_BYTE){
      data_frame_status_counter = 0;
      isDataFrameOpen= false;
      lcdDisplayDataFrameStatus();
    }
}


void loop() { // run over and over


sendDataFrame(createStatusDataFrame(START_FRAME_BYTE,0x01,random(0, 150),random(0, 150),random(0, 150),
                                    random(0, 150),random(0, 150),random(0, 150),random(0, 150),random(0, 150),
                                    0,0,0,0,0,0,0,0,0,END_FRAME_BYTE)
              ,STATUS_DATA_FRAME_SIZE);

sendDataFrame(createErrorDataFrame(START_FRAME_BYTE,0x02,random(0, 150),0,END_FRAME_BYTE)
              ,ERROR_DATA_FRAME_SIZE);

              
if(isUserFrendlySerialPreviewVisable){
Serial.println();
}


 if (softSerial_1.available()) {
    byte byteIncomingSoftSerial1 = softSerial_1.read();
    
    Serial.print(byteIncomingSoftSerial1);
    Serial.print("|");
    Serial.println();
    lcd.clear();
    parseDataFrame(byteIncomingSoftSerial1);

    
  }
  if (Serial.available()) {
    softSerial_1.write(Serial.read());
    
  }

delay(INTERVAL_BETWEEN_FRAMES);
}
